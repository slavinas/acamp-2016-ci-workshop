---
title: "AtlasCamp Dockerize it Workshop - May 2016"
---

<style>
#content {
  padding: 0 20px 0 20px;
}
img {
  width: 500px;
}

pre .CodeMirror.cm-s-neat {
  font-size: 12px;
}

#footer {
  display: none;
}

</style>

## AtlasCamp Dockerize it Workshop - May 2016

A warm welcome Matey! Prepare for a few hours of discovery and learning. This
workshop will guide you step by step but also challenge you to exercise Docker
concepts and show you how to leverage Bitbucket Pipelines to streamline your
Continuous Integration needs.

--------

**CONFIDENTIALITY PLEA**

In this workshop you'll be presented with information about a new Product - Bitbucket Pipelines - which will only be announced during the Keynote of
AtlasCamp 2016. **Please consider this information as confidential and do not
tweet, email or mention to anyone before the public announcement**.

--------

## Pre-requisites

The requirements to attend this workshop are a Bitbucket account, a Docker
installation, a text editor of your choice, Git, and the command line.

Details:

- Install Git on your system if you don't already have it: https://git-scm.com/downloads
- Create Bitbucket account if you don't already have one: http://bitbucket.org
- Install a text editor of your choice:

  * Sublime Text: https://www.sublimetext.com/
  * Atom: http://atom.io/
  * Vim, Emacs, etc.

- Login on Bitbucket
- Fork the workshop repository at: https://bitbucket.org/atlassianlabs/acamp-2016-ci-workshop
- Clone it locally
- If you are on Mac or Windows install Docker Toolbox: https://www.docker.com/products/docker-toolbox
- If you are on Linux follow the instructions for your distribution: https://docs.docker.com/engine/installation/linux/ubuntulinux/
- If you want to rely less on the venue's wifi you can pre-pull a couple of docker images by typing on a command-line:

  ```
  docker pull python:onbuild
  docker pull alpine
  docker pull busybox
  docker pull python:alpine
  ```

--------

## Part 1: Setup and familiarize with Docker

- Install the pre-requisites
- Run some Docker commands to prove your setup is working:

  - Exercise 1.1. Run a container that prints "Hello AtlasCamp!".
  - Exercise 1.2. Run a container that lists the content of a folder inside the
    cointainer.
  - Exercise 1.3. (*Optional!*): Create a text file with some text on a folder on
    the host system (your laptop). Then run a container that mounts that folder
    and outputs the contents of that text file.

**Hints:**

- For your first Docker experiments you can use a tiny image like `alpine` or `busybox`.
- The syntax to run Docker commands from the command line is:

  ```
  docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
  ```

  To have the container output text to the screen remember to add the `--tty, -t` flag.
  For commands that are interactive (like launching a shell inside a container)
  add the `--interactive, -i` (interactive) flag.

- You can invoke normal shell commands in containers by passing the flag `-c`
  to your shell of choice, for example: `/bin/sh -c "echo Hello World!"` will
  print "Hello World!" to screen and quit the shell afterwards.
- The command to list folders is `ls`.
- Mounting host volumes into running containers is done by adding the volume
  flag (`--volume, -v`) of the `run` command like the following:

  ```
  docker run -v /path/in/host:/path/inside/container
  ```


--------

## Part 2: Dockerize a web application

Your task in this section is to Dockerize the web application we provided you
with in the repository.

The repository you cloned is a Python web application. In general, once Python
applications grow they typically start to rely on few external libraries. To
handle the external dependencies many people use the standard [pip](https://en.wikipedia.org/wiki/Pip_(package_manager)
(recursive acronym which stands for "Pip Installs Packages"). Pip needs a
`requirements.txt` with a list of library names and versions needed for your
application to run.

### Dockerize it the simple way

- Step 2.1. If you haven't done it yet fork the workshop repository at
  https://bitbucket.org/atlassianlabs/acamp-2016-ci-workshop
- Step 2.2. Clone your fork of the repository locally.


- Step 2.3. Write the simplest `Dockerfile` to run the Python Flask
  application in a Docker container. Don't over think it, the simplest solution
  is only two lines long.
- Step 2.4. Build the image and tag it like `<your-initials>/acamp-workshop`
- Step 2.5. Run the image you built and remember you have to explicitly tell
  Docker run command to open the port (5000) you want to use. You can use the
  flag `--port`, `-p` to accomplish this.
- Step 2.6. If everything works out you should see on the terminal:

  ```
   * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
  ```

- Step 2.7. If you're using a non-Linux system and the Docker Toolbox - which
  behind the scenes uses Virtualbox - you have to explicitly open port `5000`
  on the tiny virtual machine running your Docker engine. To do this launch
  Virtualbox, select the `default` VM, right click `Settings...`, click on the
  `Network` icon at the top, click on the button `Port Forwarding`, click on the
  plus icon and fill in `5000` into the `Host Port` and `Guest Port` and apply
  the change.

  ![virtual box port](/static/vbox-port.png)

- Step 2.8. Point the browser to http://localhost:5000/ and you should see the
  demo application running.
- Step 2.9. Now type `docker images` and inspect how big is the image you
  have created:

  ```
  REPOSITORY                      TAG     IMAGE ID  CREATED        SIZE
  <your-initials>/acamp-workshop  latest  6b2fbb    A minute ago   683.6 MB
  ```

  You should see how much space the default Python image weighs.

**Hints:**

- Images are built from a `Dockerfile` using the command `docker build`. The
  command needs a tag (`-t`) for you to chose and the folder where the
  `Dockerfile` resides. Optionally you can specify the `Dockerfile` to read using
  `-f filename`. Example:

  ```
    docker build -t js/workshop .
  ```

- Check the documentation on how to use Python's image at `https://hub.docker.com/_/python/`
- Build the image first using the `FROM python:onbuild` image. This image is
  magical: it automatically copies the `requirements.txt` file in the right
  place and adds the entire application repository inside the container. The only
  thing left is to make sure the `Dockerfile` has a `CMD` statement that runs it.
- The command `docker images` lists all the images you have built or downloaded
  to your Docker environment.
- When you run containers using `docker run` you have to explicitly map host
  ports to the running container ports you want to open. You can do that with
  the `--port, -p` flag, which has the form `-p host-port:container-port` (i.e.
  `-p 7474:7474`).

## Part 3: Optimize your image for size

Now let's change the `Dockerfile` to use a much leaner image (we can go from
683Mb to 82Mb). If we depend on the `python:alpine` image instead than the one
we used before, we can save 600Mb.

**Note:** The `onbuild` tag automatically copies the requirements.txt file and
sources for you. The more lean `alpine` image doesn't. You need to run `pip
install` and copy the sources in the right place inside the container yourself.

In general outside of Docker to install the requirements of the Python Flask
application you would need to run:

```
pip install -r /app/requirements.txt
```

In this case you should invoke that command in a `RUN` statement in the updated
`Dockerfile`.  Also the source of our application are not automatically copied
to the container so we'll have to invoke some `COPY` command to place the
sources in the container.

- Step 3.1. Rewrite Dockerfile to use `alpine` and explicitly install requirements and
copy sources.
- Step 3.2. Build the new, smaller, image.
- Step 3.3. Check the size of the new image, which should be something like:

  ```
  REPOSITORY                      TAG     IMAGE ID   CREATED      SIZE
  <your-initials>/acamp-workshop  latest  84d89c8    9 secs ago   81.92 MB
  ```

- Step 3.4. Check that it runs correctly.
- Step 3.5. (*Optional*) Change the `Dockerfile` so that it does not re-install
  all the dependencies every time you change the sources of your application.
  To accomplish this first `ADD` the `requirements.txt` to the `/tmp` folder, run
  `pip install`, then `ADD` the rest of the sources.

**Hints:**

- Use `FROM python:alpine` instead than `FROM python:onbuild`
- The `Dockerfile` command to copy sources from the host (your laptop) to the
  container, is `COPY`.


--------

## Part 4: Build an image to run tests

In this section you will create a new `Dockerfile.test` to run our Python tests.
To run our test suite outside of a container the command would be:

  ```
  pip install -r /app/requirements-dev.txt
  ```

And then simply execute:

  ```
  py.test tests
  ```

- Step 4.1. Write a `Dockerfile.test` that will do that for you in a container.

- Step 4.2. Build the test image.

- Step 4.3. Run the test in a Docker container.

  The result should look like this:

  ```
  ================================================ test session starts =====
  platform linux -- Python 3.5.1, pytest-2.9.1, py-1.4.31, pluggy-0.3.1
  rootdir: /app, inifile:
  collected 2 items

  tests/test_greet.py ..

  ============================================== 2 passed in 0.01 seconds ==
  ```

- Step 4.4. Make a test fail now by changing one of the asserts in
  `tests/test_greet.py`, then rebuild and run the tests.

  Tests should now fail like:

  ```
  ================================================ test session starts =====
  platform linux -- Python 3.5.1, pytest-2.9.1, py-1.4.31, pluggy-0.3.1
  rootdir: /app, inifile:
  collected 2 items

  tests/test_greet.py F.

  ====================================================== FAILURES ==========
  ______________________ TestGreetingForEnglish.test_greeting_is_hello _____

  self = <tests.test_greet.TestGreetingForEnglish object at 0x7fc33e33b160>

      def test_greeting_is_hello(self):
  >       assert self.greeting == "No Hello"
  E       assert 'Hello' == 'No Hello'
  E         - Hello
  E         + No Hello
  E         ? +++

  tests/test_greet.py:15: AssertionError
  ================================= 1 failed, 1 passed in 0.03 seconds ======
  ```

--------

## Part 5: Create an image for build & test

In this section you will create a new `Dockerfile.build` that runs tests
but mounts the source code on a volume so it can be used as a build environment.
In previous steps, you created an image that copies the target code into the image.
That means you have to rebuild the image every time there is a code change.
If the purpose of the image is to test changing code,
then you can keep the code out of the image by making it a mounted volume.
Effectively, the code is an "input" to the running image,
not a fixed part of it.

- Step 5.1. Write a `Dockerfile.build` that will do that for you in a container.

- Step 5.3. Run the tests in the new Docker container, this time mounting the current directory.


--------

## Part 6: Use Pipelines to build for you

In this section,
you will turn focus to Bitbucket Pipelines.
Your task is to enable Bitbucket Pipelines for your fork of the web application.
You can do all of this through the Bitbucket web UI.

- Step 6.1. Install Bitbucket Pipelines as an add-on from the URL:
https://bitbucket-pipelines.atlassian.io/install
- Step 6.2. Navigate to your fork of the workshop repository
and begin the process to enable Pipeplines for your repository.
- Step 6.3. Enable the Pipelines feature for the repository.
- Step 6.4. Add a configuration file to your repository through Bitbucket's web interface.
- Step 6.5. Commit the configuration file and let it run.
- Step 6.6. Observe the build log to see why the build failed.
- Step 6.7. Fix the build by changing the configuration file to `pip install requirements-dev.txt` then `py.test tests`.
- Step 6.8. Observe the passing build.
- Step 6.9. Add an environment variable named `GREET_LOC` with the value `es` to the Pipelines configuration.
- Step 6.10. Re-run the build.

### Replicate running Pipelines locally

Next we want to run the tests locally by replicating Pipelines execution of Docker.

- Step 6.11. Create a local environment variables file `local.env` which
  includes the variable `GREET_LOC` and its value.
- Step 6.12. Create a local bash file for execution called `local-build.bash`.
  This script should install the dependencies using `pip` and then run the
  tests with `py.text`. Make sure to give that file execution permissions.
- Step 6.13. Issue a `docker run` command which loads up the environment
  variable (hint: use `--env-file=`) and executes the `local-build.bash` file
  you created.



**Hints:**

- Use the default Python configuration file.
This should be selected automatically for you based on the language selected for the Bitbucket repo.
- The default Python configuration file uses Tox.
You could create a `tox.ini` file but we choose to modify the configuration file for educational purposes.
- The passing build appears in many places in the Bitbucket UI:
Commits, Branches, and Pull Requests.
Plus, the Pipelines page opens up a chronological list of the build results over time.
- The environment variable does not change the test output.
It only changes the behavior of the web application.

## Appendix: Tips and Shortcuts

- To reset the entire workshop and start from scratch type:

  ```
  git reset --hard origin/master
  ```

  **Note:** If you do this you will lose all your work.

- The folder `scripts` contains a helper command to cleanup all the stale
  containers and images. Invoke it like:

  ```
  ./scripts/reset-docker.sh
  ```

You have reached the end of the workbook. Good job!

#### THE END.

